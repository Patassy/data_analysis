## Installation

I recommend to use Anaconda Python distribution. My conda environment is exported to `analysis_env.yml`

To replicate my environment, use:
`conda env create -f analysis_env.yml` (using the `-n` flag, you can give  a custom name to your environment)

to activate the environment use: `conda activate analysis_env` under Windows, or `source activate analysis_env` under Linux