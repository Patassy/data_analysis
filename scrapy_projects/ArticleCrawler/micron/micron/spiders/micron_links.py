# -*- coding: utf-8 -*-
import scrapy


class MicronLinksSpider(scrapy.Spider):
    name = 'micron_links'
    start_urls = ['http://www.micronoptics.com/2018-trb/']

    def parse(self, response):
        #for article in response.xpath('//article//a[not(ancestor::div[@class="fusion-sharing-box fusion-single-sharing-box share-box"])]/@href').re('^(?!.*http[:][/][/]www[.]micronoptics[.]com).*$'):
        for article in response.xpath('//article'):
            title = article.xpath('h2/text()').extract_first()
            if not title:
                continue
            
            yield {'title': title.strip()}

            
        next_page = response.xpath('//div[@id="main"]//div[@class="single-navigation clearfix"]/a[contains(@rel, "prev")]/@href').extract_first()
        self.log('NEXT PAGE: {0}'.format(next_page))
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)