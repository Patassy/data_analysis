# -*- coding: utf-8 -*-
from scrapy.spiders import SitemapSpider, Rule
from scrapy.linkextractors import LinkExtractor


class GetMapSpider(SitemapSpider):
    name = 'get_map'
    sitemap_urls = ['https://www.hbm.com/sitemap_hbm.xml']
    #sitemap_follow = ['^.*(/en/).*$']
    

    #rules = (Rule(LinkExtractor(deny=(re_not_hbm, '^(http|https)://')), follow=False, callback=parse_ext_links) , )
    
    def parse(self, response):
        #re_string = '^(htt)(?!.*www[.]hbm[.]com)(?!.*www[.]linkedin[.]com)(?!.*twitter[.]com)(?!.*facebook[.]com)(?!.*www[.]ncode[.]com)(?!.*www[.]hbmprenscia[.]com)(?!.*www[.]youtube[.]com).*$'        
        for page in response.xpath('//a/@href').extract():
            #self.log(page)
            if not page.startswith('http'):
                continue
            if 'www.hbm.com' in page:
                continue
            if 'www.linkedin.com' in page:
                continue
            if 'twitter.com' in page:
                continue
            if 'www.facebook.com' in page:
                continue
            if 'www.ncode.com' in page:
                continue
            if 'www.hbmprenscia.com' in page:
                continue
            if 'www.youtube.com' in page:
                continue
            


            yield {
                'orig': response.url,
                'link': page
            }
