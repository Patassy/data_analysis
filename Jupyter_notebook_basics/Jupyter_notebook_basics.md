## Jupyter notebook

Jupyter notebook is an application, that extends Python into an interactive code editor, where you can add visualizations, explanations, formulas to your code. You can even create  presentations, reports from your outputs.

> **Note:**

> - As it has been mentioned in the previous chapter, Anaconda comes pre-bundled with Jupyter notebook
> - You can find more information about Jupyter notebook on their website: [http://jupyter.org/](http://jupyter.org/)

**How to run Jupyter notebook:**

There are basically 2 ways to run Jupyter notebook:

* Launching from Anaconda navigator
	- Run Anaconda Navigator
	- Go to Home
	- Select the desired Python environment
	- Click on "Launch" under Jupyter notebook (see the screenshot below)

![](img/1_anaconda_launch_jupyter.JPG)

> **Note:**

> - This method runs Jupyter in a default folder. If my project is somewhere else, I need to navigate the software to that specific folder (not really practical) 

* Launching from the command prompt
	- Open command prompt / terminal
	- Activate the desired Python environment
	- Navigate to the desired folder and type in: jupyter notebook

![](img/2_cmd_launch_jupyter.JPG)

**Dashboard**

Once we launch Jupyter, it starts a server in the background, and shows the dashboard interface. This is the "homepage" for your notebooks.
Here you can:

- navigate through your file system
- launch and terminate notebooks
- delete notebooks
- refresh

![](img/3_jupyter_dashboard.JPG)

**Notebook basics**

Let's create a new notebook.
![](img/4_notebook_start.JPG)
A notebook can be divided into 3 parts:

- Menu
- Toolbar
- Notebook are with the cells. 2 types of cells can be used:
	- Markdown - Formulas and formatted text
	- code cells

Once you enter a cell, you can switch between 2 modes:

* Press Escape and the command mode is activated (blue marking)

![](img/8_command_mode.JPG)

* Press Enter and the edit mode is activated - you can edit the content of the cell (green marking)

![](img/9_edit_mode.JPG)

It is really helpful to use keyboard shortcuts in Jupyter notebook. Fortunately there is a button in the toolbar (called command pallette), which shows lists all shortcuts.

![](img/10_command_pallette.jpg)

Some of the most important shortcuts:

* Save notebook - ctrl + s
* create cell above - hit ESC (enter command mode) -> a
* create cell below - hit ESC (enter command mode) -> b
* delete cell - hit ESC (enter command mode) -> d -> d
* run cell - ctrl + Enter
* run cell and select next cell - shift + Enter

### Markdowns

> Markdown is a lightweight markup language with plain text formatting syntax

> Markdown is a way to style text on the web

As I've mentioned earlier, you can use the notebooks to add comments, documentation, formulas to your code - You can use the markdown language for this.
You can switch the cell type to markdown by hitting ESC (entering command mode) and hitting m.


"Working With Markdown Cells.ipynb" - This notebook contains teh basics of the markdown language, or You can use this amazing cheatsheet: [Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)


## Useful links ##
1. [http://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/what_is_jupyter.html](http://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/what_is_jupyter.html)
1. [jupyter.readthedocs.io/en/latest/install.html](jupyter.readthedocs.io/en/latest/install.html)
1. [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
1. [https://guides.github.com/features/mastering-markdown/](https://guides.github.com/features/mastering-markdown/)